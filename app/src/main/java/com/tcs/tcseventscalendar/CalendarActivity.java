package com.tcs.tcseventscalendar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener
{
    CompactCalendarView compactCalendar;
    TextView monthYearView, eventInfo;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private StorageReference mStorageReference;
    ArrayList<TCSEvent> tcsAllEvents = new ArrayList<TCSEvent>();
    ArrayList<TCSEvent> tcsEvents = new ArrayList<TCSEvent>();
    ArrayList<Event> events = new ArrayList<>();
    ArrayAdapter eventsArrayAdapter;
    ListView eventsListView;
    ProgressDialog progress;
    PDFView pdfView;
    String localFileString;

    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private int pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        mStorageReference = storage.getReference();

        Calendar cal = Calendar.getInstance();
        monthYearView = (TextView) findViewById(R.id.monthYear);
        monthYearView.setText(dateFormatMonth.format(cal.getTime()));


        compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendar.setUseThreeLetterAbbreviation(true);

        pdfView = (PDFView)  findViewById(R.id.pdfView);

        progress = new ProgressDialog(CalendarActivity.this);

        eventsListView = (ListView) findViewById(R.id.events_listview);

        eventsArrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                tcsEvents );

        myRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                compactCalendar.removeAllEvents();
                tcsAllEvents.clear();
                events.clear();
                Iterable<DataSnapshot> children = dataSnapshot.child("event").getChildren();

                for(DataSnapshot child : children)
                {
                    String title = child.child("title").getValue(String.class);
                    String desc = child.child("desc").getValue(String.class);
                    String date = child.child("date").getValue(String.class);
                    String time = child.child("time").getValue(String.class);
                    String fileName = child.child("file").getValue(String.class);
                    tcsAllEvents.add(new TCSEvent(title, desc, date, time, fileName));

                    events.add(new Event(Color.BLUE, tcsAllEvents.get(tcsAllEvents.size()-1).getEpochMillis(), tcsAllEvents.get(tcsAllEvents.size()-1).getTitle()));
                    compactCalendar.addEvent(events.get(events.size()-1));
                }
                Collections.sort(tcsAllEvents);
                getCurrentMonthEvents();
                eventsListView.setAdapter(eventsArrayAdapter);

                for (TCSEvent tcsEvent : tcsEvents) {
                    System.out.println(tcsEvent);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
            }
        });

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener()
        {
            @Override
            public void onDayClick(Date dateClicked)
            {
                compactCalendar.setCurrentSelectedDayBackgroundColor(Color.GRAY);
                getDayEvents(dateClicked);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth)
            {
                monthYearView.setText(dateFormatMonth.format(firstDayOfNewMonth));
                compactCalendar.setCurrentSelectedDayBackgroundColor(Color.TRANSPARENT);
                getCurrentMonthEvents();
            }
        });

        eventsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TCSEvent event = (TCSEvent) eventsListView.getItemAtPosition(position);
                if (permissionsGranted()) {
                    progress.setTitle("Loading Event");
                    progress.setMessage("Please Wait...");
                    progress.setCancelable(false);
                    progress.show();
                    showPDF(event);
                } else {
                    getPermissions();
                }
            }
        });
    }

    private void getDayEvents(Date dateClicked) {
        System.out.println("GET DAY EVENTS");
        Calendar c = Calendar.getInstance();
        c.setTime(dateClicked);
        c.add(Calendar.DATE, 1);
        Date end = c.getTime();
        System.out.println(dateClicked);
        System.out.println(end);

        getEvents(dateClicked, end);
    }

    private void getCurrentMonthEvents() {
        System.out.println("GET MONTH EVENTS");
        Date begin = compactCalendar.getFirstDayOfCurrentMonth();
        Calendar cal = Calendar.getInstance();
        cal.setTime(begin);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH) + 1);
        Date end = cal.getTime();
        getEvents(begin, end);
    }

    private void getEvents(Date begin, Date end) {
        System.out.println("GETTING EVENTS");
        tcsEvents.clear();
        eventsArrayAdapter.clear();
        for (TCSEvent tcsEvent : tcsAllEvents) {
            if (tcsEvent.getEpochMillis() >= begin.getTime() && tcsEvent.getEpochMillis() < end.getTime()) {
                System.out.println(tcsEvent);
                tcsEvents.add(tcsEvent);
            }
        }
    }

    private boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED;
    }

    private void showPDF(TCSEvent event) {
        String fileName = event.getFileName();
        System.out.println(fileName);
        StorageReference pdfRef = mStorageReference.child(fileName);

        //try {
            final File localFile = new File(this.getExternalCacheDir(), event.getTitle()+event.getEpochDate() + ".pdf");
            localFileString = localFile.toString();
            //localFile.createNewFile();
            //File outputDir = this.getExternalCacheDir();
            //final File localFile = File.createTempFile(event.getTitle(), ".pdf", outputDir);
            if(!localFile.exists()) {
                pdfRef.getFile(localFile)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                startPDFActivity(localFile);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                progress.dismiss();
                                Toast.makeText(CalendarActivity.this, "Something went wrong when fetching the file", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
            else
            {
                startPDFActivity(localFile);
            }

    }

    private void startPDFActivity(File localFile){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = pdfView.getWidth() / 2;
            int cy = pdfView.getHeight() / 2;

            // get the final radius for the clipping circle
            float finalRadius = (float) Math.hypot(cx, cy);

            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(pdfView, cx, cy, 0, finalRadius);


            displayFromString(localFile.toString());

            // make the view visible and start the animation
            compactCalendar.setVisibility(View.INVISIBLE);
            eventsListView.setVisibility(View.INVISIBLE);
            monthYearView.setVisibility(View.INVISIBLE);
            pdfView.setVisibility(View.VISIBLE);
            progress.dismiss();
            anim.start();
        } else {
            Intent intent = new Intent(CalendarActivity.this, PDFViewerActivity.class);
            intent.putExtra("key", localFile.toString());
            progress.dismiss();
            CalendarActivity.this.startActivity(intent);
        }
    }

    private void displayFromString(String fileName) {
        pdfView.fromUri(Uri.parse(fileName))
                .defaultPage(pageNumber)
                .enableSwipe(true)

                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", localFileString, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {


            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    private void getPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        deleteTempFiles(this.getExternalCacheDir());
    }

    @Override
    public void onBackPressed() {
        if (pdfView.getVisibility() == View.VISIBLE) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                // get the center for the clipping circle
                int cx = pdfView.getWidth() / 2;
                int cy = pdfView.getHeight() / 2;

                // get the initial radius for the clipping circle
                float initialRadius = (float) Math.hypot(cx, cy);

                // create the animation (the final radius is zero)
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(pdfView, cx, cy, initialRadius, 0);

                // make the view invisible when the animation is done
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        pdfView.setVisibility(View.INVISIBLE);
                        compactCalendar.setVisibility(View.VISIBLE);
                        eventsListView.setVisibility(View.VISIBLE);
                        monthYearView.setVisibility(View.VISIBLE);
                    }
                });

                // start the animation
                anim.start();
            }
        } else {
            super.onBackPressed();
        }
    }

    private boolean deleteTempFiles(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        deleteTempFiles(f);
                    } else {
                        f.delete();
                    }
                }
            }
        }
        return file.delete();
    }
}
