package com.tcs.tcseventscalendar;


import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TCSEvent implements Comparable<TCSEvent>
{
    private String title;
    private String desc;
    private String date;
    private String time;
    private String fileName;
    private long epochMillis;
    private long epochDate;

    public TCSEvent(String title, String desc, String date, String time, String fileName)
    {
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.time = time;
        this.fileName = fileName;

        String str = date + " " + time;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
        try
        {
            Date d = df.parse(str);
            epochMillis = d.getTime();

            d = dfDate.parse(date);
            epochDate = d.getTime();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDesc()
    {
        return desc;
    }

    public void setDesc(String desc)
    {
        this.desc = desc;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getEpochMillis()
    {
        return epochMillis;
    }

    public long getEpochDate()
    {
        return epochDate;
    }

    public String toString()
    {
        return this.title + "\n" + this.date;
    }

    @Override
    public int compareTo(@NonNull TCSEvent o) {
        return (int) (this.getEpochDate() - o.getEpochDate());
    }
}
