package com.tcs.tcseventscalendar;


import android.provider.ContactsContract;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ReadFirebaseDatabase
{
    private static final String TAG = "ReadFirebaseDatabase";
    ArrayList<TCSEvent> tcsEvents = new ArrayList<TCSEvent>();
    private final FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    public ReadFirebaseDatabase()
    {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
    }

    public ArrayList<TCSEvent> readEvents()
    {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Iterable<DataSnapshot> children = dataSnapshot.child("event").getChildren();

                for(DataSnapshot child : children)
                {
                    String title = child.child("title").getValue(String.class);
                    String desc = child.child("desc").getValue(String.class);
                    String date = child.child("date").getValue(String.class);
                    String time = child.child("time").getValue(String.class);
                    String fileName = child.child("file").getValue(String.class);
                    tcsEvents.add(new TCSEvent(title, desc, date, time, fileName));
                }

                System.out.println("INNGER: " + tcsEvents.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return tcsEvents;
    }


}
